@extends('adminlte.master')


@section('content')
<div class="card card-primary">
    <div class="card-header">
    <h3 class="card-title">Edit Pertanyaan {{$perta->id}}</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/pertanyaan/{{$perta->id}}" method="POST">
        @csrf
        @method('PUT')
      <div class="card-body">
        <div class="form-group">
          <label for="title">Judul</label>
        <input type="text" class="form-control" id="judul" name="judul" value=" {{old('judul', $perta->judul)}} " placeholder="Masukkan Judul">
          @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group">
          <label for="body">Isi</label>
          <textarea name="isi" id="isi" class="form-control placeholder="isi">{{old('isi', $perta->isi)}}</textarea>
          @error('isi')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Create</button>
      </div>
    </form>
  </div>
@endsection