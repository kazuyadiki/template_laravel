<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create(){
        return view('pertanyaan.create');
    }

    public function store(Request $request){
            //dd($request->all());
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required'
        ]);
        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
         ]);
            
         return redirect('/pertanyaan')->with('Success', 'Pertanyaan Berhasil Disimpan');
 
    }

    public function index(){
        $pertanyaan = DB::table('pertanyaan')->get();
        //dd($posts);
        return view('pertanyaan.index', compact('pertanyaan'));
    }

    public function show($id){
        $perta = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.show', compact('perta'));
    }

    public function edit($id){
        $perta = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.edit', compact('perta'));
    }

    public function update($id, Request $request){
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required'
        ]);
        
        $query = DB::table('pertanyaan')
                    ->where('id', $id)
                    ->update([
                        'judul' => $request['judul'],
                        'isi' => $request['isi']
                    ]);
        return redirect('/pertanyaan')->with('Success', 'Berhasil Update Pertanyaan!');
    }
    public function destroy($id){
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan')->with('Success', 'Post Berhasil dihapus');
    }
}
